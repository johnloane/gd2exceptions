/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author administrator
 */
public class Main {
    public static void main(String[] args) {
//        int x = 2;
//        int y = 5;
        //System.out.println(divide(x,y));
//        System.out.println(divideLBYL(x,y));
//        System.out.println(divideEAFP(x,y));
        int x = getIntEAFP();
        System.out.println("x is " + x);
    }
    
    //need the simplest function that will throw an exception
    private static int divide(int x, int y){
        return x/y;
    }
    
    private static int divideLBYL(int x, int y){
        if(y != 0){
            return x/y;
        }
        else{
            return 0;
        }
    }
    
    private static int divideEAFP(int x, int y){
        try{
            return x/y;
        }
        catch(ArithmeticException e){
            return 0;
        }
        
            
    }

    private static int getIntLBYL() {
        Scanner sc = new Scanner(System.in);
        int input = 0;
        boolean isValid = false;
        
        while(!isValid){
            System.out.println("Please enter an integer ");
            if(sc.hasNextInt()){
                input = sc.nextInt();
                return input;
            }
            else{
                sc.next();
            }
        }
        return input;
        
    }
    
    private static int getIntLBYLV2() {
        Scanner sc = new Scanner(System.in);
        boolean isValid = true;
        
        System.out.println("Please enter an integer: ");
        String input = sc.next();
        for(int i=0; i < input.length(); ++i){
            if(!Character.isDigit(input.charAt(i))){
                isValid = false;
                break;
            }
        }
        if(isValid){
            return Integer.parseInt(input);
        }
        return 0;  
    }
    
    private static int getIntEAFP(){
        Scanner sc = new Scanner(System.in);
        int input;
        while(true){
            System.out.println("Please enter an integer");
            try{
                input = sc.nextInt();
                return input;
            }
            catch(InputMismatchException e){
                sc.next();
            }
        }
        
    }
    
}
